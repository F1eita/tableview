package com.example.etubuildings

import android.graphics.Color
import android.view.View
import android.widget.ImageView

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle

import java.util.Random


class MainActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
    }
    val random = Random()

    fun rand(from: Int, to: Int) : Int {
        return random.nextInt(to - from) + from
    }
    fun getRandomPic(): Int{
        return rand(1, 8)
    }
    fun getRandomColor(): Int{
        return rand(0, 256)
    }
    fun onClick(view : View){
        val r = getRandomColor()
        val g = getRandomColor()
        val b = getRandomColor()
        val pic = getRandomPic()
        val image : ImageView = findViewById(R.id.imageView)
        var button = view
        when (pic){
            1 -> image.setImageResource(R.drawable.building1)
            2 -> image.setImageResource(R.drawable.building2)
            3 -> image.setImageResource(R.drawable.building3)
            4 -> image.setImageResource(R.drawable.building4)
            5 -> image.setImageResource(R.drawable.building5)
            6 -> image.setImageResource(R.drawable.building6)
            7 -> image.setImageResource(R.drawable.building7)
        }
        button.setBackgroundColor(Color.rgb(r, g, b))
    }
}